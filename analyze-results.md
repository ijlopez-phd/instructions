
# Analyze the results of a simulation

<br>
## Description of the log file

For each simulation a log file is generated. By default, this is stored in `/var/log/agency-services`. Unfortunately, currently this path can only be changed via the source code. If you need to do so, in the repository _EnergyAgents_,  you have to change the value of the variable `AUCTION_LOGGING_PATH` of the class `EnergyAgentsGlobals`. After that, you have to compile and install again the library.

The log file contains information that helps to know the agreements reached by each agent. Some examples are shown below in order to help the user to understand the format of the log files. The most basic example is:

    i1B646,df0,-/1.0
    i1B645,df0,-/1.0

The file contains one line per house of the model (which also means one line per software agent). In a specific line, the fields are separated by commas. Basically, each line contains:

1. The name of the _house_ (or the agent). In the above example, houses are called _i1B646_ and _i1B645_.
2. The name of the directory facilitator to which the house belongs. The directory facilitator is a yellow-pages service that helps to contact the software agents that command the houses.
3. The list of agreements reached by the house. There is an agreement for each auction block. In the above example, there is only one auction block.

The list of agreements contains the bulk of the information. In the simplest case, it looks like the previous example. The dash symbol means that the house did not participate in any auction; while the subsequent number stands for the level of the OADR signal that finally had to apply the house. Accordingly, in the previous case both agents did not participate in any auction and applied level signals with value `1.0` (that is, they applied signals of the type _Moderate_).

A more complex case is shown below:

    c03,df0,C-100.0/100.0/0.0#100.0-120.0

In this example:

* The agent did participate in one block of auctions.
* The letter _C_ means that agent participated as _Consumer_. The _P_ would mean _Producer_.
* The string `100.0/115.0/0.0` means that: a) the agents bought 100 units; b) the agent was willing to buy 115 units; c) the house finally had to apply a level signal with value `0.0` (normal consumption).
* The sharp sign is used to inform about the price at which the energy was bought. In this case, the string `100.0-120.0` means that 100 unit were bought at the price of 120.

In the following example, the required units (100) were bought at two blocks of prices. The first 50 units were bought at the price of $120, and the another 50 units at $150. As shown, the prices sections are separated by semicolons.

    c03,df0,C-100.0/100.0/0.0#50.0-120.0;50.0-150.0


In the following example, the house participates in three blocks of auctions. In all of them it participates as _Consumer_. In the first two blocks, the agent purchases 50 units at the price of $120; while in the third block, the agent purchases 150 units at the price of $160.

    c01,df0,C-50.0/50.0/0.0#50.0-120.0,C-50.0/50.0/0.0#50.0-120.0,C-150.0/150.0/0.0#150.0-160.0

In the following example, the agent participates as Producer in one block of auctions.

    p01,df0,P-150.0/170.0/2.0

Specifically, the string `P-150.0/170.0/2.0` means that: a) the agent sold 150.0 units; b) it was offering 170.0 units; c) it finally had to apply a signal level with value 2.0 (critical level of consumption).

<br>
## Tool to extract information from the log file

The application provides the script `report.py` in order to analyze the information of the log files. The script can be found in the repository _AgencyServices_:

    /resources/scripts

The information is printed in console. If you add the argument `--pretty` to the call, the fields are formatted so that humans can easily understand the information. Conversely, when that argument is not specified, all fields are shown in CSV format. In this mode, the output is intended to be used as input of other program.

A typical call using the _pretty_ mode could be:

    pretty.py auc_ign_1_1_0.0-0.log.1 --pretty

Which shows:

	N nodes:  629 

	N consumers:  289 [93, 98, 98] 

	N producers:  1182 [394, 394, 394] 

	Exchanged:  11672.394 

	Demanded:  12653.235 

	Offered:  36259.868 

	Covered:   92.25%

	Provided:  32.19%

	Cost:  1403698.523 

	Unit cost:  120.258 

	Empty auctions:  0 [0, 0, 0] 

	Cancelled auctions:  11 [1, 5, 5] 

	Lost consumers:  11 [1, 5, 5] 

	Lost producers:  784 [286, 260, 238] 

	Overbookers:  0  (0.00%)

	Overbooking mean:  0.00%

Without using the _pretty_ mode, the script would show:

	629, 289, 1182, 1, 784, 11, 0, 11, 0, 0.00, 11672.39, 12653.24, 36259.87, 120.26,11672.39;12653.24;36259.87;1403698.52;120.26


The information provided by script can be easily changed. So, users are encouraged to change the source code if necessary.