
# Configuration

<br>
## Basic description

The project requires two configuration files. We recommend to create a specific folder to this end (we use `/etc/agency-services`). The two required files are:

 * _energyagents.config_: This is the main configuration file. This contains the URLs related to the servers to be deployed, as well as the information required to use and configure external resources.

 * _df.properties_: This file sets the maximum number of agents that can be instantiated by the framework Jade. This file is required because the default number proposed by Jade is too low for running simulations typical of the Smart Grid.

The default content of the file "_energyagents.config_" is:

		# Grid Operator
		gridop.url=http://localhost:9090/gridop
		gridop.servicesPoint=/gridop/resources

        # ASPEMs
		aspem.initPort=8080
		aspem.contextPath=/aspem
		aspem.servicesPoint=/aspem/resources
		#aspem.resource=../aspem/target/aspem-1.0.war
		aspem.resource=/home/ijlopez/workspaces/phd/cloudyem/aspem/target/aspem-1.0

		# XMPP Server
		xmpp.server.url=localhost
		xmpp.server.port=5222
		xmpp.user.login=gridoperator
		xmpp.user.pass=gridoperator1
		xmpp.user.resource=asmarket

		# Jade Platform
		jade.df.path=/etc/agency-services/df.properties

As shown, the last line sets the path of the other configuration file. Its default content is:

		jade_domain_df_maxresult = 9999

It sets the maximum number of agents to 9999. In case more agents are needed, this number should be increased. However, the user should be warned that Jade is not prepared for such large number of agents.


<br>
## Loading the configuration file

The file "_energyagents.config_" can be omitted. In that case, the default values shown above are used. However, the file "_df.properties_" must be defined, as the API of Jade requires it. Therefore, if the file "_energyagents.config_" is not defined, then it is assumed that the path of the Jade's configuration file is `/etc/agency-services/df.properties`.

In case the user wants to override some of the above values, a new file must be created. The path of this new file can be specified by using the environment variable `GRIDOP_CONFIG`.

		$ GRIDOP_CONFIG=/etc/agency-services/energyagents.config


<br>
## Properties of the configuration file

 * _gridop.url_: Url where server corresponding to the Grid Operator is instantiated.
 * _gridop.servicesPoint_: Path of the services layer of the Grid Operator.

 * _aspem.initPort_: Port from which the instances of the ASPEMs are created. The first ASPEM is attached to the port specified in this property. This number increases successively with the new instances.
 * _aspem.contextPath_: Path of the application in the ASPEM servers.
 * _aspem.servicesPoint_: Path of the services layer of the ASPEMs.
 * _aspem.resource_: In file system, path of the WAR file containing the code of the ASPEM application. Assuming that the code of the repositories corresponding to the Grid Operator and the ASPEMs is deployed at the same level of the file system, the value `../aspem/target/aspem-1.0.war`should be valid. It is also possible to set the path of the source code of the ASPEM application.

 * _xmpp.server.url_: Url of the XMPP server.
 * _xmpp.server.port_: Port used in the XMPP server for the communications.
 * _xmpp.user.login_: Valid account name. This is the account that uses the Grid Operator for the communications through XMPP.
 * _xmpp.user.pass_: Password of the XMPP acount.
 * _xmpp.user.resource_: String that describes the Jabber resource.

 * _jade.df.path_: Path of the configuration file of the Jade framework.
 