# Create a new market-based scenario

<br>
## Adapting an existing GLM file

This section explains how to modify existing GLM files in order for they work with the new module. Through this procedure, you can automatically add market information to the nodes IEEE13 and IEEE14, so they can be simulated in the platform.

The conversion process is carried out through Python scripts located in the repository  _Asgridlab_:

    /resources/scripts/

It is important to add this folder to the PATH of the system in order for the scripts can be executed from any place.

The script to convert GLM files is `fulfill.py`. A brief description of the available options can be obtained by typing `fulfill.py --help`. The basic command to convert the node IEEE13 is shown below (the file `IEEE_13_house.glm`is provided by GridLAB-D). 

    fulfill.py IEEE_13_house.glm

As a result, the file `out.glm`is generated. It contains the definition of:

* The module `agencyservices`.
* The objects `GridOperator` and `ASPEM`.
* An object of the type `ASBox` in each house.

It is important you to open and read this file. Furthermore, the path wherein the script is executed must contain the following files (all of them can be copied from the folder `/src/main/resources/glm` of the repository _gridop_):

* light\_schedule.glm
* water\_and_setpoint\_schedule.glm
* WA-Seattle.tmy2
* light\_schedule.glm
* sch\_producer-less-1.glm, sch\_producer-less-2.glm, sch\_producer-less-3.glm, sch\_producer-less-4.glm

Moreover, you may want to change:

* The definition of the object `clock`. It defines the period of simulation and the timezone. For intance, we usually set the timezone to `GMT0`.
* The field `scenarioCode` of the object `agencyservices`. This code must match the code assigned to a scenario in the Grid Operator's application.
* The number of ASPEMs that are created.

By default, the information related to the market of auctions is not added. The following command generates a file with this information:

    fulfill.py IEEE_13_house.glm --set-auction=1 --pmin=80 --pmax=150 --profile=2

As a result, the ASBox objects added to the _houses_ contains information such as the role they adopt in the market (producer or consumer) and the prices of their offers.

__Before using the scenario__ in later simulations, it is necessary to create and store demand estimations of the scenario. This task is explained in the following section.

<br>
## Generating demand estimations for a scenario

Software agents make use of demand estimations in order to plan their future actions. The demand estimations are calculated as the average demand resulting of simulating several times the scenario.

When you convert a GLM via the script `fullfil.py` you can choose to generate log files for each _house_ object. These log files are used by other script in order to calulate the demand estimations for each house and store it in the database system. Specifically, in order for the GLM file generates log files for each house, the parameter `--record` of the script `fulfill.py` must be set to the value `0`or `2`. You can also set the target folder via the parameter `--data`. By default its value is set to `.data` (note that the dot before the name makes the folder is hidden). For instance, the previous call would be as follows:

    fulfill.py IEEE_13_house.glm --set-auction=1 --pmin=80 --pmax=150 --profile=2 --record=2 --data=logs

Creating the demand estimation can be a tedious task that involves simulating several times the same scenario and managing the generated log files.  In order to free the user from these tasks, the script `batchsim.py`is provided. A description of the arguments that accepts the script can be obtained by typing `batchsim.py --help`. As a quick example, if you want to simulate five times the IEEE13 scenario, you have to run:

    batchsim.py IEEE_13_house.glm --nsims=5

The simulations executed through this script do not use the layer of software agents. So, the Grid Operator's web application is not necessary during this process. This is because calculating demand estimations are based on standard simulations of GridLAB-D.

Once the batch process has finished, the average demand for each house is calculated and stored in the database system by using the script `storeforecast.py`. __It should be noted__ that the script assumes that: a) the database system is PostgreSQL and is installed in _localhost_; b) the database is called _agencyservices_; c) and the login and password to access this database are `agencyservices` (user) and `agencyservices` (password). If you want to change any of these conditions, you have to modify the subroutine `connect_db`of the script.

A typical call to the script `storeforecast.py` is shown below:

    storeforecast.py --code=ieee13 ./.data

The parameter `--code` defines the name of the table in which the data have to be stored. This table is also created by the script. The argument (`./data` in the previous example) specifies the folder wherein the log files are located. __It is important to note__ that the name of the table (the parameter `--code`) is the name that must be used to fulfill the field _Store name_ in the Grid Operator's web application, in the web form for creating a new scenario (via the Grid Operator's application).


