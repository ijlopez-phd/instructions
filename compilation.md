# Instructions for compiling the source code of the projects

The source code of the projects is available in the Gitlab group [_ijlopez-phd_](https://gitlab.com/ijlopez-phd)

The repositories can be organized in two major modules:

1. Repositories related to the simulation of the electrical grid. The source code of these projects is written in C++.
   * [Wonrest](https://gitlab.com/ijlopez-phd/wonrest)
   * [Asgridlab](https://gitlab.com/ijlopez-phd/asgridlab)

2. Repositories related to the _cloud_ layer of software agents that bargain of behalf of users in electrical markets. These repositories are written in Java.
   * [Gridop](https://gitlab.com/ijlopez-phd/gridop)
   * [ASPEM](https://gitlab.com/ijlopez-phd/aspem)
   * [EnergyAgents](https://gitlab.com/ijlopez-phd/energyagents)
   * [SimpleDR](https://gitlab.com/ijlopez-phd/simpledr)

<br>
<br>
## Step 1: Download the source code.

The code of the following repositories can be downloaded by using `git`, or by using the web interface provided by [Gitlab](https://gitlab.com/groups/ijlopez-phd). Specifically, in the page of each repository the user can click on the _Download_ button to get a compressed file containing the more recent version of the source code.

As for the projects related to the _cloud_ layer of software agents, it is recommend to uncompress them under the same root folder. In particular, we recommend that the structure of folders after uncompressing repositories is as follows:

    + cloud-layer
        + gridop
        + aspem
        + energyagents
        + simpledr

With regard to the projects related to the simulation of the electrical grid, the repository '_Asgridlab_' must be installed as one more plugin in the root path of GridLAB-D; while 'Wonrest can be uncompressed wherever the user wants.

<br>
<br>
## Step 2: Compile

To compile the Java repositories is necessary to have installed in the system:

 * The [Java Development Kit (JDK)](http://www.oracle.com/technetwork/es/java/javasebusiness/downloads/index.html). All the process commented in this section have been carried out using the version 1.7.0_51.

 * The software project management [Maven](http://maven.apache.org/).

The user can check whether these software packages are correctly installed by using the commands:

        java -version
        mvn --version

It is also necessary to have connection to Internet because the project management Maven resolves dynamically all the dependencies, downloading them when necessary.

With regard to the projects written in C++, the user can check the tools necessary to compile GridLAB-D. Furthermore, the library `libcurl` must be installed in the system.


<br>
### Compiling the repository 'SimpleDR'

Enter the folder of the repository _SimpleDR_ and run the following commands:

        mvn jaxb2:generate
        mvn package -DskipTests

At this point, the library `simpledr-0.5.jar` is created into the directory `target`. Since this library is used by others projects, this must be installed as a Maven dependency by running the command:

        mvn install:install-file -Dfile=./target/simpledr-0.5.jar -DgroupId=es.siani -DartifactId=simpledr -Dversion=0.5 -Dpackaging=jar

<br>
### Compiling the repository 'EnergyAgents'

The _EnergyAgents_ project depends on the libraries of the project [JADE](http://jade.tilab.com/), which are not available on the official Maven repositories. So, it is necessary to install them manually as Maven dependencies. The name of the library to be installed follows the pattern of `jade-4.x.x.jar`. At the moment of writing this document, the latest stable version is 4.3.1.

Assuming that the file `jade-4.3.1.jar` is located in the folder `./lib`, the command to install the version 4.3.1 of Jade as a Maven dependency is:

        mvn install:install-file -Dfile=./lib/jade-4.3.1.jar -DgroupId=com.tilab -DartifactId=jade -Dversion=4.3.1 -Dpackaging=jar
    
If the version of Jade is other than 4.3.1, then the new version number must be updated in the file `pom.xml` that is located in the folder of the repository _Energyagents_. Specifically, the line that must be changed is:

        <jade.version>4.3.1</jade.version> 
 

Enter the folder of the repository _EnergyAgents_ and run the following command to generate the library:

        mvn package -DskipTests
    
Install the package as a Maven dependency by running the command:

        mvn install:install-file -Dfile=target/energyagents-0.5.jar -DgroupId=es.siani -DartifactId=energyagents -Dversion=0.5 -Dpackaging=jar

<br>
### Compiling the repository 'ASPEM'

Enter the folder of the repository _ASPEM_ and run the following command:

        mvn package -DskipTests

<br>
### Compiling the repository 'Gridop'

Enter the folder of the repository _Gridop_ and run the following command:

        mvn package -DskipTests

<br>
### Compiling the repository 'Wonrest'

_Wonrest_ is a C++ library to facilitate the access to RESTful services.

The compilation and installation of _Wonrest_ is carried out by using GNU Autotools. Specifically, it is necessary to have previously installed the GNU utility programs: _Autoconf_, _Automake_ and _Libtool_. Furthermore, Wonrest depends on the library `libcurl`. It is recommended to use the version 7.26.0 or latter.

        autoreconf -isf
        ./configure
        ./make
        ./make install

By default, the generated libraries are installed in the path `/usr/local/lib`.

<br>
### Compiling the repository 'asgridlab'

This repository is an add-on for the version 2.3 of the simulator GridLAB-D. So, this must be previously installed in the system. The folder containing the code of the add-on must be uncompressed at the root path of GridLAB-D. Type the following commands to compile and install the add-on:

        autoreconf -isf
        ./configure --prefix=/usr --libdir=/usr/lib/gridlabd
        ./make
        ./make install
    
It is assumed that libraries of GridLAB-D have been installed into the path `/usr/lib/gridlabd`. Otherwise, in the `configure` command the destination path of the parameter `libdir` should be changed.

In order for the plugin is compiled along the others modules of GridLAB-D, the file `configuration.ac` located in the rootpath of GridLAB-D must be updated. Specifically, the path of the Makefile corresponding to the new plugin must be added to the variable `AC_CONFIG_FILES`. In this way, whenever GridLAB-D is recompiled, it will also contain the libraries of the new plugin.