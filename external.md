
# External resources

<br>
## GridLAB-D simulator

The platform uses [GridLAB-D](http://www.gridlabd.org/) in order to simulate the electrical grid. Specifically, the version 2.3 is used. **It is important to highlight** that that the modules have not been tested with the version 3.0, which is the current stable version of GridLAB-D (it had not been released at the time of developing the project).

GridLAB-D has an active developer community and forums where users can ask and find all necessary information. Useful links on GridLAB-D can be found [here](http://www.gridlabd.org/resources.stm).

<br>
## XMPP Server

The communication between the simulator and software agents is carried out using the XMPP protocol, as indicated in the OpenADR 2.0 standard. To this end, we use the server [Openfire](http://www.igniterealtime.org/projects/openfire/), although any other standard server is valid.

The server must be configured in order for users can automatically create new accounts. In practice, by using the user interface provided by the software package, it is only necessary to create the account of the Grid Operator. In this regard, the default configuration file expects the following data:

  * __Username__ : gridoperator
  * __Password__: gridoperator1
  * __Resource__: asmarket

Later, the application that implements the Grid Operator creates dynamically the users required by each simulation. This is why is so important that the server is configured so that users (and in special the Grid Operator user) are allowed to create new accounts.

Furthermore, the default configuration file also sets the following values for the XMPP server:

  * __Url__: localhost
  * __Port__: 5222

If any of the above values is not going to be used, a new configuration file must be created. See the [Configuration](/configuration.md) section.

<br>
## Database system

The application uses two databases for the simulations. One is for storing control information on entities such as _scenarios_ and _simulations_. The other database stores demand estimations used by the software agents in order to plan their actions.

It is recommended to use [PotsgresSQL](http://www.postgresql.org/) to store the information. Specially, PostgreSQL is important to store the demand estimations, which consist of a large amount of data. In case you want to use another database system to this end, the Python scripts that create and store the estimations should be changed.

In order to test the application, the installation of  [HyperSQL (HSQLDB)](http://hsqldb.org/) is also recommended. This is a light database system that facilitates testing the application with little overhead. However, it should be noted that HSQLDB is not a valid storage system for the demand estimations, as it is unable to handle so much data.

By default, PostgreSQL is used for both to store the control information and the demand estimations, even though this condition can be easily changed in the configuration files. See [Database](/database.md).
