
This file contains the instructions for installing, configuring and running the software projects related to the PhD of Ignacio Lopez-Rodriguez.

# Instructions

  * [Compilation](/compilation.md)

  * [General configuration](/configuration.md)

  * [Installation of external modules](/external.md)

  * [Configuration of the database system](/database.md)

  * [Verification of the installation](/testing.md)

  * [Generating a new scenario](/generate-scenario.md)

  * [Running simulations](/basic-simulation.md)

  * [Obtaining simulation results](/analyze-results.md)
