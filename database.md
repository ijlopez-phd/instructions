# Creating the structure of the database

The application uses two databases for the simulations. One is used for storing control information on entities such as _scenarios_ and _simulations_. The other database stores demand estimates used by the software agents in order to plan their actions. Instructions to create both of them are provided below.

<br>
## Database for storing control information

It is necessary to install a database system. By default, the application is configured to use [PotsgresSQL](http://www.postgresql.org/), although other systems are also possible. In this case, the driver of the new database system must be added to the `pom.xml` file of the project in order for Maven downloads it. By default, the application is also configured to download the drivers of [HyperSQL (HSQLDB)](http://hsqldb.org/).

Next, the application must be configured to use the chosen database system. This task is carried out through the file `/src/main/webapp/WEB-INF/mybatis/config.xml`. There, the user has to set up the `properties`section.

	<property name="driver" value="org.postgresql.Driver" />
		<property name="url" value="jdbc:postgresql://localhost/gridoptest" />	
		<property name="username" value="gridoptest" />
		<property name="password" value="gridoptest1" />
	</properties>

As shown, in the default configuration:

 * The PostgreSQL database system is used.
 * The database system is installed in _localhost_.
 * The name of the database is _gridoptest_.
 * The name of the user to access the database is _gridoptest_, who has the password _gridoptest1_.

In case you want to use HSQLDB, you may want to use:

	<properties>
		<property name="driver" value="org.hsqldb.jdbc.JDBCDriver" />
		<property name="url" value="jdbc:hsqldb:hsql://localhost/gridoptest;default_schema=true" />	
		<property name="username" value="SA" />
		<property name="password" value="" />
	</properties>


Assuming that the default settings are used, the database "_gridoptest_" must be created. The next step is to create the tables required by the application. The SQL commands to do so can be found in the files located at the path `/src/main/java/resources/database`. As can be seen, commands for PostgreSQL (`postgres.sql`) and HSQLDB (`hsqldb.sql`) are provided. If another database system is used, the SQL commands of either file must be exported.

<br>
## Database for storing demand estimations

It is highly recommend to use PostgreSQL for storing demand estimations. Database systems such as HyperSQL are not an option due to the large amount of data handled. A database with the following settings must be created:

 - __Database name__: agencyservices

 - __User__: agencyservices

 - __Password__: agencyservices

Demand estimates are created by taking statistics from simulations that are executed iteratively by a Python script. This process is described in a later section.