
# Running a basic simulation

Before running any simulation, you must remember that the following applications must be running:

* The XMPP server
* The database system
* The Grid Operator application

Specifically, to execute the Grid Operator application, type the following command once you are in the folder corresponding to the _gridop_ repository:

    MAVEN_OPTS = "-Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=512m"
    mvn jetty:run-exploded -Djetty.port=9090

Next, you have to create a new _Scenario_ item via the user interface of the application ([http://localhost:9090/gridop](http://localhost:9090/gridop)). The steps to do so are:

1. On the _Navigation_ menu, click on the link _Scenarios_.
2. Click on the button _New_ shown above the list of available scenarios (the first time this list is empty).
3. Create a new scenario called _basic_. Use the following information to fulfill the fields:
 * __Code__: basic
 * __Store name__: basic
 * __Program__: Upload the file `/src/main/resources/programs/basic.xml`
 * __Description__: (Any text)

The GLM file of the scenario (the GridLAB-D file that describes the electrical grid) is `/src/main/resources/glm/basic.glm`. So, once you are on that folder, you must type the following command to run the simulation:

    gridlabd basic.glm

__Note__: In case the name of the host of the XMPP server is not _localhost_, you can change it in the GLM file. It is defined via the property `xmppUrl` of the element `agencyservices`.
