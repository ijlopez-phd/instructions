# Check the installation

Once all repositories have compiled and external resources have been installed, it is recommend to check that all components are running and interacting properly. In this section, simple commands are shown to verify the servers and the plugin developed for extending GridLAB-D.

<br>
## Check the servers that represent the Grid Operator and the ASPEMs

Before proceeding, the user must check that:

 * The XMPP server is operative.
 * The binaries of Maven must be available in the command line. It can be checked by running the command `mvn --version`.

The server instances of the Grid Operator and the ASPEMs require more memory than usual. Accordingly, in order to set up the Java Virtual Machine, the environment variable `MAVEN_OPTS` must be defined with the following value:

    MAVEN_OPTS = "-Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=512m"

Next, in order to launch the layer of software agents, the user must enter the folder of the repository "_gridop_" and run the following command:

    mvn jetty:run-exploded -Djetty.port=9090

If the Grid Operator's application is running properly, its user interface should be accessible via the URL:

    http://localhost:9090/gridop


To check the services layer, type the following URL in the browser. As a result, the word `pong`should appear in the screen.

    http://localhost:9090/gridop/resources/ping


<br>
## Check the GridLAB-D simulator

To check if the plugin _Asgridlab_ is installed correctly, the following command should display the description of the new components. Among them, it should appear in the console the description of the objects: `ASBox`, `ASPEM`and `GridOperator`.

    gridlabd --modhelp agencyservices
